import java.nio.*;

public class RawPackage
{
    public int type;
    public int data1;
    public int data2;
    public int data3;
    public int data4;

    public static RawPackage getRawPackage(byte[] bytes)
    {
        RawPackage r_pack = new RawPackage();
        ByteBuffer bBuffer = ByteBuffer.wrap(bytes);
        bBuffer.order(ByteOrder.LITTLE_ENDIAN);

        r_pack.type = bBuffer.get() & 0xFF;
        r_pack.data1 = bBuffer.get();
        r_pack.data2 = bBuffer.get();
        r_pack.data3 = bBuffer.get();
        r_pack.data4 = bBuffer.getInt();

        return r_pack;
    }
}
